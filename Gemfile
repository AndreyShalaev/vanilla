source 'https://rubygems.org'

gem 'rails', '~> 3.2.19'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem 'pg', '~> 0.17.1'

# Auth
gem 'authlogic', '~> 3.4.2'
gem 'bcrypt', '~> 3.1.7'
gem 'scrypt', '1.2.1'

gem 'simple_form', '~> 2.1.1'

# File Uploading
gem 'paperclip', '~> 4.2.0'
gem 'paperclip-meta', '~> 0.4.3'

# API
gem 'acts_as_api', '~> 0.4.1'

gem 'thin', '~> 1.5.1'
gem 'faye', '~> 0.8.9'

gem 'autoprefixer-rails', '~> 2.1.1.20140710'

# State machine
gem 'workflow', '~> 1.0.0'

# Meta search
gem 'squeel', '~> 1.1.0'

gem 'has_scope', '~> 0.5.1'

gem 'russian', '~> 0.6.0'

gem 'acts_as_paranoid', '~> 0.4.0'

gem 'i18n-js', '~> 3.0.0.rc5'

# Model decorators
gem 'draper', '~> 1.0'

group :development do
  gem 'better_errors', '~> 1.1.0'
  gem 'letter_opener', '~> 1.2.0'
  gem 'rails_best_practices', '~> 1.15.4'
  gem 'annotate', '~> 2.6.5'
end

group :test, :development do
  gem 'rspec-rails', '~> 2.0'
end

group :test do
  gem 'factory_girl_rails', '~> 4.4.1'
  gem 'database_cleaner', '~> 0.9.1'
end

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  gem 'uglifier', '~> 1.3.0'

  gem 'jquery-rails', '~> 3.0.0'

  gem 'angularjs-rails', '~> 1.0.7'

  gem 'angularjs-rails-resource', '~> 0.2.0'

  gem 'zurb-foundation', '~> 4.3.1'

  gem 'font-awesome-rails', '~> 3.2.1.3'

  # jQuery asset pipeline plugins
  gem 'jquery-fileupload-rails', '~> 0.4.1'
  gem 'rails-timeago', '~> 2.0'
  #gem 'select2-rails', '~> 3.2.0'
end
