# == Schema Information
#
# Table name: photos
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  photo_image_id :integer
#  description    :text
#

require 'spec_helper'

describe Photo do

  let!(:user) { FactoryGirl.create(:user) }

  let!(:attrs) {
    {
      image: File.open(Rails.root.join('spec', 'support', 'big_image.jpg')),
      user: user
    }
  }

  it 'should create a valid instance given valid attrs' do
    Photo.create!(attrs)
  end

  it 'required author' do
    Photo.new(attrs.merge(user: nil)).should_not be_valid
  end

  it 'reqired image' do
    Photo.new(attrs.merge(image: nil)).should_not be_valid
  end
end
