# encoding: utf-8
class CreateImages < ActiveRecord::Migration
  def self.up
    create_table :images do |t|
      t.attachment :img
      t.text :img_meta
      t.string :img_fingerprint
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :images
  end
end
