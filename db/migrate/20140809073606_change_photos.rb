# encoding: utf-8
class ChangePhotos < ActiveRecord::Migration
  def self.up
    drop_attached_file :photos, :image
    remove_column :photos, :image_meta
    add_column :photos, :photo_image_id, :integer
    add_column :photos, :description, :text

    remove_index :photos, :user_id
    add_index :photos, [:user_id, :photo_image_id], :unique => true
  end

  def self.down
    add_attached_file :photos, :image
    add_column :photos, :image_meta, :text
  end
end
