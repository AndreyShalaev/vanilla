# encoding: utf-8
# == Schema Information
#
# Table name: images
#
#  id               :integer          not null, primary key
#  img_file_name    :string(255)
#  img_content_type :string(255)
#  img_file_size    :integer
#  img_updated_at   :datetime
#  img_meta         :text
#  img_fingerprint  :string(255)
#  type             :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Image < ActiveRecord::Base
  # Public: максимальный размер изображения
  MAX_IMAGE_SIZE = 10
  # Public: путь к картинке по-умолчанию
  DEFAULT_IMAGE_URL = '/system/images/:class/:id_partition/:id_:style.:extension'
  # Public: Дозволенные типы файлов
  ALLOWED_IMAGE_EXTENSIONS = %w(image/jpg image/jpeg image/png image/gif image/pjpeg)

  attr_accessible :img

  def self.attachment_styles
    {
      :original => {
        :geometry => '1920x1080>',
        :format   => 'jpg',
        :animated => false
      }
    }
  end

  def self.attachment_options
    {
      :use_file_command => true,
      :default_url => '/assets/:class/default_:style.jpg',
      :convert_options => {
        :all => '-strip'
      },
      :styles => attachment_styles,
      :url => DEFAULT_IMAGE_URL
    }
  end
end
