# == Schema Information
#
# Table name: photos
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  photo_image_id :integer
#  description    :text
#

class Photo < ActiveRecord::Base
  attr_accessible :photo_image, :user, :description

  belongs_to :user
  belongs_to :photo_image

  validates :user, :presence => true
  validates :photo_image, :presence => true

  delegate :url, :img_width, :img_height, :img_fingerprint, :img, :to => :photo_image, :prefix => false

  acts_as_api

  api_accessible :angular do |t|
    t.add :id
    t.add lambda { |photo|
      {
        small: { url: photo.url(:small), width: photo.img_width(:small), height: photo.img_height(:small) },
        medium: { url: photo.url(:medium), width: photo.img_width(:medium), height: photo.img_height(:medium) },
        large: { url: photo.url(:large), width: photo.img_width(:large), height: photo.img_height(:large) },
        thumb: { url: photo.url(:thumb), width: photo.img_width(:thumb), height: photo.img_height(:thumb) }
      }
    }, :as => :image
  end

end
