# encoding: utf-8
# == Schema Information
#
# Table name: images
#
#  id               :integer          not null, primary key
#  img_file_name    :string(255)
#  img_content_type :string(255)
#  img_file_size    :integer
#  img_updated_at   :datetime
#  img_meta         :text
#  img_fingerprint  :string(255)
#  type             :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

#
# Public: Изображение профиля
class ProfileImage < Image

  has_many :profiles, :inverse_of => :photo, :foreign_key => 'photo_id', :class_name => 'Profile'

  def self.attachment_styles
    super.merge({
      :large => {
        :geometry => '800x',
        :format   => 'jpg',
        :animated => false
      },
      :medium => {
        :geometry => '220x',
        :format   => 'jpg',
        :animated => false
      },
      :thumb => {
        :geometry => '120x',
        :format   => 'jpg',
        :animated => false
      },
      :small => {
        :geometry => '50x',
        :format   => 'jpg',
        :animated => false
      }
    })
  end

  def self.attachment_options
    super.merge({
      :convert_options => {
        :medium => '-strip -gravity north -crop 220x340+0+0 +repage',
        :small => '-strip -gravity north -crop 50x50+0+0 +repage',
        :large => '-strip',
        :thumb => '-strip -gravity north -crop 120x90+0+0 +repage'
      },
      :styles => attachment_styles
    })
  end

  has_attached_file :img, attachment_options

  validates_attachment_presence :img
  validates_attachment_content_type :img, :content_type => ALLOWED_IMAGE_EXTENSIONS
  validates_attachment_size :img, :less_than => MAX_IMAGE_SIZE.megabytes

  delegate :url, :to => :img
  delegate :height, :width, :to => :img, :prefix => true

  acts_as_api

  api_accessible :angular do |t|
    t.add :id
    t.add lambda { |photo|
      {
        small: { url: photo.url(:small), width: photo.img_width(:small), height: photo.img_height(:small) },
        medium: { url: photo.url(:medium), width: photo.img_width(:medium), height: photo.img_height(:medium) },
        large: { url: photo.url(:large), width: photo.img_width(:large), height: photo.img_height(:large) },
        thumb: { url: photo.url(:thumb), width: photo.img_width(:thumb), height: photo.img_height(:thumb) }
      }
    }, :as => :image
  end
end
