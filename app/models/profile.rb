# == Schema Information
#
# Table name: profiles
#
#  id         :integer          not null, primary key
#  first_name :string(255)      default(""), not null
#  last_name  :string(255)      default(""), not null
#  user_id    :integer
#  birthday   :date
#  gender     :string(255)      default("male"), not null
#  photo_id   :integer
#  bio        :text             default(""), not null
#

class Profile < ActiveRecord::Base

  attr_accessible :first_name, :last_name, :bio, :birthday, :gender, :photo

  belongs_to :photo, :class_name => 'ProfileImage', :foreign_key => 'photo_id', :inverse_of => :profiles
  belongs_to :user

  validates :first_name, :length => {:in => 3..15 }, :allow_blank => true
  validates :last_name, :length => {:in => 3..15}, :allow_blank => true
  validates :gender, inclusion: {:in => %w(male female)}, allow_blank: true
  validates :bio, :length => {:maximum => 6000}

  after_save :prevent_duplicate_photos, :if => :photo_id_changed?

  delegate :url, :img_width, :img_height, :img_fingerprint, :img, :to => :photo, :allow_nil => true, :prefix => true

  def avatar(size = :medium)
    photo_url(size) || "/assets/user/default_#{size}.jpg"
  end

  def avatar_width(size = :medium)
    photo_img_width(size) || case size
      when :small then 50
      when :medium then 200
      when :thumb then 120
      else 800
    end
  end

  def avatar_height(size=:medium)
    photo_img_height(size) || case size
      when :small then 50
      when :medium then 200
      when :thumb then 90
      else 800
    end
  end

  def update_photo(photo)
    update_attributes :photo => photo
  end

  # Public: Возраст пользователя
  #
  # Returns Integer
  def age
    return 0 unless birthday.present?

    now = Time.zone.now.to_date
    now.year - birthday.year - ((now.month > birthday.month || (now.month == birthday.month && now.day >= birthday.day)) ? 0 : 1)
  end

  private

  def prevent_duplicate_photos
    duplicates = ProfileImage.where(:img_fingerprint => photo_img_fingerprint)
                             .where('id <> ?', photo_id)

    unless duplicates.empty?
      profile_ids = duplicates.map { |d| d.profiles.map(&:id) }.flatten.uniq.delete(id)
      self.class.update_all({:photo_id => photo_id}, {:id => profile_ids})
      duplicates.delete_all
    end
  end
end
