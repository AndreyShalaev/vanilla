class Api::PhotosController < ApiController

  def create
    @photo = current_user.photo_images.build(:img => params[:photo])

    if @photo.save
      respond_with @photo, :api_template => :angular, :location => api_photo_url(@photo)
    else
      respond_with @photo.errors
    end
  end

  def show
    @photo = Photo.find(params[:id])

    respond_with @photo, api_template: :angular, location: api_photo_url(@photo)
  end

end
