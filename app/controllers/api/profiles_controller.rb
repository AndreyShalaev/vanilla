class Api::ProfilesController < ApiController
  # GET /api/profile
  def show
    respond_with current_user, :api_template => :angular
  end

  # PUT /api/profile
  def update
    current_user.profile.update_attributes!(params[:profile].except(:id, :email, :avatar, :fullname, :username))

    render_for_api :angular, :json => current_user
  end

  # Public: Обновление изображения профиля
  def update_photo
    current_user.profile.build_photo(:img => params[:photo])

    if current_user.profile.save
      respond_with current_user.profile.photo, :api_template => :angular, :location => api_photo_url(current_user.profile.photo)
    else
      respond_with current_user.profile.errors
    end
  end
end
